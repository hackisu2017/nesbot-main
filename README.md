This is a machine-learning bot desinged to learn how to play any given level of super mario bros for the NES.

Through use of a neural network it decides on an output based on the game state. This output is determined by a web of neurons that have weights determining its likelyhood of choosing any given output.
After a simulation ends, this instace of the neural network is given a "fitness score" based on its completion in the level. The higher the fitness score, the more likely it will be the basis for future species.
Random mutations will modify the weights associated with different neurons in order to evolve.

Jon Edlund
Nick Cretors
Mark Bowler
Erik Shouse
