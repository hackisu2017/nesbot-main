local evolve = {}
genomes = {}


function evolve.printT(t)
	for i=1, #t do
		io.write(t[i].." ")
	end
	print()
end

print()

function evolve.concat(a, b)
	out = {}
	for i=1,#a do
		out[#out+1] = a[i]
	end
	
	for i=1, #b do
		out[#out+1] = b[i]
	end
	
	return out
end

function genGenome(species, f)
	out = {}
	for i=1, #species do
		out[i].gene = species[i]
		out[i].fitness = f(species[i])
	end
	return out
end

function evolve.newGeneration(genomes, f, breed)
	table.sort(genomes, f)
	
	for i=1, #genomes do
		if math.random()<(i-1)/(#genomes-1) then
			genomes[i].fitness = 0
		end	
	end
	dead = {}
	survivors = {}
	for i=1, #genomes do
		if genomes[i].fitness == 0 then
			dead[#dead+1] = 0
		else
			survivors[#survivors+1] = genomes[i]
		end
	end

	for i=1, #dead do
		dead[i] = breed(survivors[math.random(1,#survivors), survivors[math.random(1,#survivors)])
	end
	
	return evolve.concat(survivors, dead)
	
end

