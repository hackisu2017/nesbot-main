local network = {}

math.randomseed(os.time())

outputs = {1, 2, 3, 4, 5, 6}
bias = .5

function network.randGrid(x, y)
	 t = {}
	 for i=1,x*y do
	     t[i] = math.random()
	 end
	 return t
end

function network.newNeuron()
	 local neuron = {}
	 neuron.value = 0.0
	 neuron.weights = {}
	 neuron.incoming = {}
	 return neuron
end

function network.sigmoid(x) return 1/(1 + math.exp(-x)) end

function network.calcValue(neuron)
	 sum = 0
	 for i=1,#neuron.connections do
	     sum = sum + (neuron.connections[i] * neuron.weights[i])
	 end
	 sum = sum - bias
	 return sigmoid(sum)
end


function network.generateNetwork(inputSize, seed)
	 if seed ~= nil then
	    math.randomseed(seed)
	 end
	 
	 local out = {}
	 out.neurons = {}
	 out.numInputs = inputSize

	 for i=1,inputSize do
	     out.neurons[i] = network.newNeuron()
	 end

	 numNeurons = math.random(6, 50)
	 for i=1,numNeurons do
	     out.neurons[inputSize+i] = network.newNeuron()
	 end

	 for i=1,6 do
	     out.neurons[inputSize+numNeurons+i] = network.newNeuron()
	 end

	 for i=1,numNeurons do
	     numInc = math.random(2, 12)
	     for j=1,numInc do
	     	 out.neurons[inputSize+i].incoming[j] = math.random(1, 170-i)
		 out.neurons[inputSize+i].weights[j] = (math.random() * 2) - 1
	     end
	 end

	 for i=1,6 do
	     numInc = math.random(2, 12)
	     for j=1,numInc do
	     	 out.neurons[inputSize+numNeurons+i].incoming[j] =
		 					math.random(1, 170+numNeurons)
		 out.neurons[inputSize+numNeurons+i].weights[j] = (math.random() * 2) - 1
	     end
	 end
	 return out
	
end

function network.mutate(in, change)
	 for i=170,#in.neurons do
	     for j=1,#in.neurons[i].weights do
	     	 in.neurons[i].weights[j] = in.neurons[i].weights[j] + 
		 		math.min(1,
					math.max(-1,
						change*(math.random()+math.random()-1)))
	     end
	 end

	 doAdd = math.random()
	 doRemove = math.random()
	 toAdd = 0
	 toRemove = 0

	 if doAdd > .9 then
	    if doAdd > .95 then
	       toAdd = 2
	    else
	       toAdd = 1
	    end
	 end
	 if doRemove > .9 then
	    if doRemove > .95 then
	       toRemove = 2
	    else
	       toRemove = 1
	    end
	 end

	 for i=1,toAdd do
	     place = math.random(170, #in.neurons - 6)
	     temp = in.neurons[place]
	     in.neurons[place] = network.newNeuron()
	     for k=place+1,#in.neurons do
	     	 temp2 = in.neurons[k]
		 in.neurons[k] = temp
		 temp = temp2
	     end
	     numInc = math.random(2, 12)
	     for j=1,numInc do
	     	 in.neurons[place].incoming[j] = math.random(1, 170-place)
		 in.neurons[place].weights[j] = (math.random() * 2) - 1
	     end
	 end
end

function network.remove(in)
	 for i=1,toRemove do
	     place = math.random(170, #neurons - 6)
	     for j=place,#in.neurons - 1 do
	     	 in.neurons[j] = in.neurons[j+1]
	     end
	     in.neurons[#in.neurons] = nil
	     for j=#in.neurons - 4, #in.neurons do
	     	 for k=1,#in.neurons[j].incoming do
		 end
	     end
end

return network
		 
--[[terminate = 1

while terminate do
      xsize = 5
      ysize = 5
      inputs = randGrid(xsize, ysize)
      network = generateNetwork(inputs)
      for i=1,#network.neurons do
      	  io.write(i)
	  io.write(": ")
      	  io.write(network.neurons[i].value)
	  io.write("\n")
      end
      terminate = nil
end
--]]
